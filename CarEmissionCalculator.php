<?php
require_once("EmissionCalculator.php");

define("BIODIESEL_ADVANTAGE_FACTOR", 0.36);
define("SPEED_DISADVANTAGE_LIMIT", 120);
define("SPEED_DISADVANTAGE_FACTOR", 0.25);

class CarEmissionCalculator extends EmissionCalculator {

    public $carEmission;

    public function calculateCarEmission() {
        $carEmission = ceil($this->distance * CAR_MULTIPLY_FACTOR);

        if ($this->speed > SPEED_DISADVANTAGE_LIMIT) {
            $carEmission = $carEmission * SPEED_DISADVANTAGE_FACTOR;
        }

        if ($this->fuel === "Biodiesel") {
            $carEmission = ceil($carEmission - ($carEmission * BIODIESEL_ADVANTAGE_FACTOR));
        }

        if ($this->persons > 1) {
            $carEmission = $carEmission / $this->persons;
        }

        return $carEmission;
    }
}
?>