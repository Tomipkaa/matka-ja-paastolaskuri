<?php
session_start();

class Journey {
    function __construct() {
        if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === "POST") {
            $_SESSION["vehicleType"] = filter_input(
                INPUT_POST, "vehicleType", FILTER_SANITIZE_STRING);
            $_SESSION["distance"] = filter_input(
                INPUT_POST, "distance", FILTER_SANITIZE_NUMBER_INT);
        }
    }
}

$journey = new Journey();

if ($_SESSION["vehicleType"] === "Auto") {
    header("Location: car.html");
}
else {
    header("Location: Calculator.php");
}

?>