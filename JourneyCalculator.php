<?php
require_once("Calculator.php");

class JourneyCalculator extends Calculator {
    private $hours;
    private $minutes;
    private $seconds;
    private $time;

    public function calculateTime()
    {
        $distanceInMeters = $this->distance * 1000;
        $speedInMetersPerSecond = $this->speed * 0.27777777777777777777777777777;
        $this->seconds = $distanceInMeters / $speedInMetersPerSecond;
        $this->hours = floor($this->second / 3600);
        $this->seconds -= $this->hours * 3600;
        $this->minutes = floor($this->seconds / 60);
        $this->seconds = floor($this->seconds - ($this->minutes * 60));

        $this->time = $this->hours . " h " . $this->minutes . " min " . $this->seconds . " sek ";
        return $this->time;
    }
}
?>